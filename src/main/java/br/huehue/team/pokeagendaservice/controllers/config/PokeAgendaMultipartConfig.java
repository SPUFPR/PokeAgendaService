/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers.config;

import br.com.caelum.vraptor.observer.upload.DefaultMultipartConfig;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Specializes;

/**
 *
 * @author adams
 */
@Specializes
@ApplicationScoped
public class PokeAgendaMultipartConfig extends DefaultMultipartConfig {
    
    public long getSizeLimit() {
        return 1024L * 1024L * 100L;
    }
    
    public long getFileSizeLimit() {
        return 1024L * 1024L * 100L;
    }
}
