/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;
import br.huehue.team.pokeagendaservice.controllers.annotations.AuthenticationRequired;
import br.huehue.team.pokeagendaservice.controllers.response.PokeAgendaObjectResponse;
import br.huehue.team.pokeagendaservice.dao.PokemonDao;
import br.huehue.team.pokeagendaservice.dao.PokemonFavoritoDao;
import br.huehue.team.pokeagendaservice.dao.TreinadorDao;
import br.huehue.team.pokeagendaservice.model.PesquisaDTO;
import br.huehue.team.pokeagendaservice.model.Pokemon;
import br.huehue.team.pokeagendaservice.model.PokemonFavorito;
import br.huehue.team.pokeagendaservice.model.Treinador;
import br.huehue.team.pokeagendaservice.utils.PokePhotoStore;
import java.io.File;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author adams
 */
@Controller
public class PokemonController extends AbstractController {

    @Inject
    private HttpServletRequest request;

    @Inject
    private TreinadorDao tdao;

    @Inject
    private PokemonDao pkdao;

    @Inject
    private PokemonFavoritoDao pfdao;

    private final Validator validator;

    public PokemonController() {
        this(null);
    }

    @Inject
    public PokemonController(Validator validator) {
        this.validator = validator;
    }

    private Treinador getTreinador() {
        try {
            return tdao.findByLogin(request.getHeader("X-PokeAgenda-User"));
        } catch (Exception ex) {
            return null;
        }
    }

    @AuthenticationRequired
    @Post("/pokemon")
    public void create(Pokemon pokemon, UploadedFile photo) {
        if (Objects.nonNull(photo)) {
            System.err.println(photo.getFileName());
            System.err.println(photo.getContentType());
        } else {
            System.err.println("NO PHOTO!!!!!");
        }
        try {
            validator.validate(pokemon).onErrorUse(Results.json());
            List<PesquisaDTO> pcheck = pkdao.findByName(pokemon.getNome());
            if (pcheck.size() > 0) {
                //pokemon já existe. 
                PesquisaDTO pp = pcheck.get(0);
                error("Pokemon " + pp.getNome() + " já foi cadastrado pelo treinador " + pp.getCriador());
            } else {
                if (Objects.nonNull(photo)) {
                    PokePhotoStore pps = new PokePhotoStore(request.getServletContext(), photo);
                    pps.store();
                    pokemon.setFoto(pps.getGeneratedFilename());
                }
                pokemon.setCriador(getTreinador());
                System.err.println(pokemon.toString());
                pkdao.create(pokemon);
                created();
            }
        } catch (Exception ex) {
            if (validator.hasErrors()) {
                error("Erro de validação de dados", validator.getErrors());
            } else {
                error("Ocorreu um erro ao processar sua requisição.");
            }
        }
    }

    @AuthenticationRequired
    @Get("/pokemon/{id}")
    public void get(Long id) {
        try {
            Pokemon pk = pkdao.get(id);
            this.response = new PokeAgendaObjectResponse(200, "Pokemon encontrado", pk);
            respond();
        } catch (Exception ex) {
            error("Pokemon solicitado não existe");
        }
    }

    @AuthenticationRequired
    @Get("/pokemon")
    public void list() {
        try {
            List<PesquisaDTO> pokemons = pkdao.findByName(null);
            this.response = new PokeAgendaObjectResponse(200, "Lista de pokemons cadastrados", pokemons);
            respond();
        } catch (Exception ex) {
            error("Ocorreu um erro ao processar sua requisição.");
        }
    }

    @AuthenticationRequired
    @Post("/pokemon/search")
    public void search(String nome) {
        try {
            System.err.println(nome);
            List<PesquisaDTO> pk = pkdao.findByName(nome);
            this.response = new PokeAgendaObjectResponse(200, "Resultados da pesquisa", pk);
            respond();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
            error("Erro ao executar pesquisa.");
        }
    }

    @AuthenticationRequired
    @Get("/pokemon/{id}/bookmark")
    public void bookmark(Long id) {
        try {
            Treinador treinador = getTreinador();
            if (Objects.nonNull(treinador.getFavorito())) {
                System.err.println("Removendo pokemon favorito anterior");
                pfdao.remove(treinador.getFavorito());
            }
            PokemonFavorito pf = new PokemonFavorito();
            pf.setPokemon(pkdao.get(id));
            pf.setTreinador(treinador);
            pfdao.create(pf);
            created();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            error("Ocorreu um erro ao processar a sua requisição.");
        }
    }

    @AuthenticationRequired
    @Get("/pokemon/{id}/unbookmark")
    public void unbookmark(Long id) {
        try {
            Treinador treinador = getTreinador();
            if (Objects.nonNull(treinador.getFavorito())) {
                System.err.println("Removendo pokemon favorito anterior");
                pfdao.remove(treinador.getFavorito());
            }
            accepted();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            error("Ocorreu um erro ao processar a sua requisição.");
        }
    }

    //@AuthenticationRequired
    @Get("/pokemon/{id}/picture")
    public File getPicture(Long id) {
        try {
            Pokemon pokemon = pkdao.get(id);
            return new File(request.getServletContext().getRealPath(String.format("/uploaded_images/%s", pokemon.getFoto())));
        } catch (Exception ex) {
            return null;
        }
    }
}
