/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.huehue.team.pokeagendaservice.controllers.response.PokeAgendaObjectResponse;
import br.huehue.team.pokeagendaservice.dao.TreinadorDao;
import br.huehue.team.pokeagendaservice.model.Treinador;
import br.huehue.team.pokeagendaservice.utils.PasswordUtils;
import java.util.Objects;
import javax.inject.Inject;

/**
 *
 * @author adams
 */
@Controller
public class AuthController extends AbstractController {

    @Inject
    private TreinadorDao tdao;

    @Post("/login")
    public void login(String login, String senha) {
        try {
            if (Objects.nonNull(login)) {
                System.err.println("Login: "+login);
                System.err.println("Senha: "+senha);
                Treinador tcheck = tdao.findByLogin(login);
                String senhaCrypto = PasswordUtils.getHashedPassword(senha);
                if (!senhaCrypto.equals(tcheck.getSenha())) {
                    throw new Exception();
                }
                this.response = new PokeAgendaObjectResponse(200, "Autenticado com sucesso", tcheck);
                respond();
            } else {
                throw new Exception();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            error("Ocorreu um erro ao autenticá-lo. Verifique seus dados e tente novamente.");
        }
    }

}
