/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers.interceptors;

import br.com.caelum.vraptor.AfterCall;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.BeforeCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.AcceptsWithAnnotations;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.caelum.vraptor.view.Results;
import br.huehue.team.pokeagendaservice.controllers.annotations.AdminRequired;
import br.huehue.team.pokeagendaservice.dao.TreinadorDao;
import br.huehue.team.pokeagendaservice.model.Treinador;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author adams
 */
@Intercepts
@RequestScoped
@AcceptsWithAnnotations(AdminRequired.class)
public class AdminRequiredInterceptor {
 
    @Inject
    private HttpServletRequest request;
    
    @Inject
    private Result result;
    
    @Inject
    private TreinadorDao dao; 
   
    @BeforeCall
    public void before() {
        System.err.println("Admin privileges required for "+request.getRequestURI());
    }
    
    @AroundCall
    public void intercept(SimpleInterceptorStack stack) {
        try {
            Treinador tc = dao.findByLogin(request.getHeader("X-PokeAgenda-User"));
            if(tc.isAdmin()) {
                stack.next();
            } else {
                throw new Exception();
            }
        } catch(Exception ex) {
            result.use(Results.status()).forbidden("Insufficient privileges");
        }
    }
    
    @AfterCall
    public void after() {
        
    }
}
