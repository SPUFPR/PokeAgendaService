/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers;

import br.com.caelum.vraptor.Consumes;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.validator.Validator;
import br.com.caelum.vraptor.view.Results;
import br.huehue.team.pokeagendaservice.controllers.annotations.AdminRequired;
import br.huehue.team.pokeagendaservice.controllers.annotations.AuthenticationRequired;
import br.huehue.team.pokeagendaservice.controllers.response.PokeAgendaObjectResponse;
import br.huehue.team.pokeagendaservice.dao.TreinadorDao;
import br.huehue.team.pokeagendaservice.model.Treinador;
import br.huehue.team.pokeagendaservice.utils.PasswordUtils;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author adams
 */
@Controller
public class TreinadorController extends AbstractController {

    @Inject
    private HttpServletRequest request;

    @Inject
    private TreinadorDao tdao;

    private final Validator validator;

    public TreinadorController() {
        this(null);
    }

    @Inject
    public TreinadorController(Validator validator) {
        this.validator = validator;
    }

    private Treinador getTreinador() {
        try {
            return tdao.findByLogin(request.getHeader("X-PokeAgenda-User"));
        } catch (Exception ex) {
            return null;
        }
    }

    @AuthenticationRequired
    @AdminRequired
    @Consumes("application/json")
    @Post("/treinador")
    public void create(Treinador treinador) {
        try {
            validator.validate(treinador).onErrorUse(Results.json());
            treinador.setSenha(PasswordUtils.getHashedPassword(treinador.getSenha()));
            tdao.update(treinador);
            created();
        } catch (PersistenceException pex) {
            error("Treinador já existe!");
        } catch (Exception ex) {
            if (validator.hasErrors()) {
                error("Erro de validação de dados", validator.getErrors());
            } else {
                error("Ocorreu um erro ao processar sua requisição.");
            }
        }
    }

    @AuthenticationRequired
    @Get("/treinador/bookmarked")
    public void bookmarks() {
        try {
            Treinador treinador = getTreinador();
            this.response = new PokeAgendaObjectResponse(200, "Favorito", treinador.getFavorito().getPokemon());
            respond();
        } catch (Exception ex) {
            error("Ocorreu um erro ao processar a sua requisição");
        }
    }
}
