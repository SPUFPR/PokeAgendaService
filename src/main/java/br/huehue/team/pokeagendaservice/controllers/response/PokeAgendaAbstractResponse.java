/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers.response;

import br.huehue.team.pokeagendaservice.model.AbstractEntity;
import java.io.Serializable;

/**
 *
 * @author adams
 */
public abstract class PokeAgendaAbstractResponse<T> implements Serializable {
    private Integer code; 
    private String message;
    private T data;
    
    public PokeAgendaAbstractResponse(Integer code, String message, T data) {
        this.code = code; 
        this.message = message; 
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
}
