/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.huehue.team.pokeagendaservice.controllers.response.PokeAgendaAbstractResponse;
import br.huehue.team.pokeagendaservice.controllers.response.PokeAgendaObjectResponse;
import javax.inject.Inject;

/**
 *
 * @author adams
 */
public abstract class AbstractController {

    @Inject
    private Result result;

    protected PokeAgendaAbstractResponse response;

    protected void respond() {
        result.use(Results.json()).indented().from(response, "pokews-response").include("data").serialize();
    }

    protected void error() {
        this.error("Um erro ocorreu ao processar a sua requisição");
    }

    protected void error(String message) {
        this.error(message, null);
    }

    protected void error(String message, Object data) {
        this.response = new PokeAgendaObjectResponse(500, message, data);
        respond();
    }

    protected void success() {
        this.success("Requisição processada com sucesso!");
    }

    protected void success(String message) {
        this.success(message, null);
    }

    protected void success(String message, Object data) {
        this.response = new PokeAgendaObjectResponse(200, message, data);
        respond();
    }
    
    protected void created() {
        this.response = new PokeAgendaObjectResponse(201, "", null);
        respond();
    }
    
    protected void accepted() {
        this.response = new PokeAgendaObjectResponse(202, "", null);
        respond();
    }
    
}
