/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.controllers.response;

/**
 *
 * @author adams
 */
public class PokeAgendaObjectResponse extends PokeAgendaAbstractResponse<Object> {
    
    public PokeAgendaObjectResponse(Object data) {
        this(200, "Requisição processada", data);
    }
    
    public PokeAgendaObjectResponse(Integer code, String message, Object data) {
        super(code, message, data);
    }
    
}
