/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author adams
 */
public class PasswordUtils {

    public static String getHashedPassword(String plainPassword) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            return byte2str(generateHash(plainPassword, md));
        } catch (NoSuchAlgorithmException nsae) {
            return null;
        }
    }
    
    private static String byte2str(byte[] source) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < source.length; ++i) {
            int ival = source[i] & 0xff;
            if (ival < 16) {
                sb.append("0");
            }
            sb.append(Integer.toHexString(ival));
        }
        return sb.toString();
    }

    private static byte[] generateHash(String str, MessageDigest md) {
        md.reset();
        md.update(str.getBytes());
        return md.digest();
    }
}
