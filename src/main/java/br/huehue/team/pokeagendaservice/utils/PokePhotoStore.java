/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.utils;

import br.com.caelum.vraptor.observer.upload.UploadedFile;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import javax.servlet.ServletContext;

/**
 *
 * @author adams
 */
public class PokePhotoStore {

    private ServletContext context;
    private UploadedFile uploadedFile;
    private String generatedFilename; 

    public PokePhotoStore(ServletContext context, UploadedFile uploadedFile) {
        this.context = context;
        this.uploadedFile = uploadedFile;
    }

    private void checkBasePath() {
        File file = new File(context.getRealPath("/uploaded_images/"));
        if(!file.exists()) {
            file.mkdirs();
        }
    }
    
    private String genStorePath() {
        String fileExtension = ".bmp";
        switch(uploadedFile.getContentType()) {
            case "image/jpeg":
            case "image/jpg":
                fileExtension = "jpg";
                break;
            case "image/png":
                fileExtension = "png";
                break;
        }
        generatedFilename = String.format("%s.%s", UUID.randomUUID().toString(),fileExtension);
        return context.getRealPath(String.format("/uploaded_images/%s", generatedFilename));
    }
    
    public void store() throws IOException {
        checkBasePath();
        File destFile = new File(genStorePath());
        uploadedFile.writeTo(destFile);
    }

    public String getGeneratedFilename() {
        return generatedFilename;
    }

    public void setGeneratedFilename(String generatedFilename) {
        this.generatedFilename = generatedFilename;
    }    
}
