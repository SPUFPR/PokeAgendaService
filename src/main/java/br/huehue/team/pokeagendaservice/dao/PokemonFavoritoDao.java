/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.dao;

import br.huehue.team.pokeagendaservice.model.PokemonFavorito;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author adams
 */
@RequestScoped
public class PokemonFavoritoDao extends GenericDao<PokemonFavorito, Long> {

    public PokemonFavoritoDao() {
        super(PokemonFavorito.class);
    }
}
