/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.dao;

import br.huehue.team.pokeagendaservice.model.Treinador;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.persistence.criteria.Predicate;

/**
 *
 * @author adams
 */
@RequestScoped
public class TreinadorDao extends GenericDao<Treinador, Long> {
    
    public TreinadorDao() {
        super(Treinador.class);
    }
    
    public Treinador findByLogin(String login) throws Exception {
        Predicate p = this.cb.and();
        if(Objects.nonNull(login)) {
            p.getExpressions().add(this.cb.equal(this.from.get("login"), login));
            return this.singleResult(p);
        } else {
           throw new Exception(); 
        }
    }
    
}
