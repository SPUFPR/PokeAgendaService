/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.dao;

import br.huehue.team.pokeagendaservice.model.PesquisaDTO;
import br.huehue.team.pokeagendaservice.model.Pokemon;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author adams
 */
@RequestScoped
public class PokemonDao extends GenericDao<Pokemon, Long> {

    public PokemonDao() {
        super(Pokemon.class);
    }
    
    public List<PesquisaDTO> findByName(String name) throws PersistenceException {
        CriteriaQuery<PesquisaDTO> cq = this.cb.createQuery(PesquisaDTO.class);
        Root<Pokemon> from = cq.from(entityClass);
        cq.select(
                cb.construct(
                        PesquisaDTO.class,
                        from.get("id"),
                        from.get("nome"),
                        from.get("especie"),
                        from.get("foto"),
                        from.get("altura"),
                        from.get("peso"),
                        from.get("criador").get("nome")
                )
        );
        if(Objects.nonNull(name)) {
            Predicate p = this.cb.like(from.<String>get("nome"), name + "%");
            cq.where(p);
        }
        List<Order> order = new ArrayList<>();
        order.add(cb.asc(from.get("nome")));
        cq.orderBy(order);
        TypedQuery<PesquisaDTO> tq = em.createQuery(cq);
        return tq.getResultList();
    }
}
