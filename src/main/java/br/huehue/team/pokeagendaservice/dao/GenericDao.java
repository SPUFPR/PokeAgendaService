/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.dao;

import br.huehue.team.pokeagendaservice.model.AbstractEntity;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

/**
 *
 * @author adams
 * @param <T>
 * @param <I>
 */
@Dependent
public abstract class GenericDao<T extends AbstractEntity, I extends Number> {

    @Inject
    protected EntityManager em;

    protected CriteriaBuilder cb;

    protected CriteriaQuery<T> cq;

    protected Root<T> from;

    protected final Class<T> entityClass;

    public GenericDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @PostConstruct
    void postConstructTrigger() {
        this.cb = em.getCriteriaBuilder();
        this.cq = cb.createQuery(entityClass);
        this.from = this.cq.from(entityClass);
    }

    /**
     * Find an entity
     *
     * @param id Entity's ID
     * @return
     * @throws PersistenceException
     */
    public T get(I id) throws PersistenceException {
        T entity = em.find(entityClass, id);
        if (Objects.isNull(entity)) {
            throw new EntityNotFoundException();
        }
        return entity;
    }

    /**
     * Persists an entity
     *
     * @param entity
     * @return
     * @throws PersistenceException
     */
    @Transactional
    public T create(T entity) throws PersistenceException {
        return em.merge(entity);
    }

    /**
     * Persists an entity
     *
     * @param entity
     * @return
     * @throws PersistenceException
     */
    @Transactional
    public T update(T entity) throws PersistenceException {
        return this.create(entity);
    }

    /**
     * Removes an entity
     *
     * @param entity
     * @throws PersistenceException
     */
    @Transactional
    public void remove(T entity) throws PersistenceException {
        T e = em.find(entityClass, entity.getId());
        em.remove(e);
    }

    /**
     * Returns a single result using a custom criteria
     *
     * @param p Where Clause predicates
     * @return
     * @throws PersistenceException
     */
    public T singleResult(Predicate p) throws PersistenceException {
        return this.singleResult(p, null);
    }

    /**
     * Returns a single result using a custom critera/order
     *
     * @param p Where Clause predicates
     * @param order Order'S Clause
     * @return
     * @throws PersistenceException
     */
    public T singleResult(Predicate p, List<Order> order) throws PersistenceException {
        if (Objects.nonNull(p)) {
            cq.where(p);
        }
        if (Objects.nonNull(order)) {
            cq.orderBy(order);
        }
        TypedQuery<T> tq = em.createQuery(cq);
        tq.setMaxResults(1).setFirstResult(0);
        return tq.getSingleResult();
    }

    /**
     * Executes a query using a custom criteria/order for a collection of
     * entities
     *
     * @param p Where Clause predicates
     * @param order Order's Clause
     * @return
     * @throws PersistenceException
     */
    public List<T> execute(Predicate p, List<Order> order) throws PersistenceException {
        return this.execute(p, order, 0, 0);
    }

    /**
     * Executes a paginated query using a custom criteria/order for a collection
     * of entities
     *
     * @param p Where Clause predicates
     * @param order Order's Clause
     * @param limit Number of max results
     * @param offset Offset from where to start
     * @return
     * @throws PersistenceException
     */
    public List<T> execute(Predicate p, List<Order> order, int limit, int offset) throws PersistenceException {
        if (Objects.nonNull(p)) {
            cq.where(p);
        }
        if (Objects.nonNull(order)) {
            cq.orderBy(order);
        }
        TypedQuery<T> tq = em.createQuery(cq);
        if (limit > 0) {
            tq.setMaxResults(limit).setFirstResult(offset);
        }
        return tq.getResultList();
    }

    /**
     * Queries the number of existent entities
     *
     * @return
     * @throws PersistenceException
     */
    public Long count() throws PersistenceException {
        return this.count(null, false);
    }

    /**
     * Queries the number of distinct existent entities
     *
     * @return
     * @throws PersistenceException
     */
    public Long countDistinct() throws PersistenceException {
        return this.countDistinct(null);
    }

    /**
     * Queries the number of distinct existent entities for a given criteria
     *
     * @param p Where Clause predicates
     * @return
     * @throws PersistenceException
     */
    public Long countDistinct(Predicate p) throws PersistenceException {
        return this.count(p, true);
    }

    /**
     * Queries the number of existent entities, distinct or not, for a given
     * criteria
     *
     * @param p Where Clause predicates
     * @param distinct Distinct flag
     * @return
     * @throws PersistenceException
     */
    public Long count(Predicate p, boolean distinct) throws PersistenceException {
        CriteriaBuilder myCb = em.getCriteriaBuilder();
        CriteriaQuery<Long> myCq = myCb.createQuery(Long.class);
        Root<T> myFrom = myCq.from(entityClass);
        if (distinct) {
            myCq.select(myCb.countDistinct(myFrom));
        } else {
            myCq.select(myCb.count(myFrom));
        }

        if (Objects.nonNull(p)) {
            myCq.where(p);
        }
        TypedQuery<Long> tq = em.createQuery(myCq);
        return tq.getSingleResult();
    }
}
