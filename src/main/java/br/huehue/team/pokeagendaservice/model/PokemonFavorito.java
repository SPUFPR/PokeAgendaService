/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author adams
 */
@Entity
@Table(name = "pokemon_favoritos",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"treinador"})
        }
)
public class PokemonFavorito extends AbstractEntity<Long> {

    @ManyToOne(optional = false)
    @JoinColumn(name = "pokemon", nullable = false)
    private Pokemon pokemon;
    @OneToOne
    @JoinColumn(name = "treinador", nullable = false)
    private Treinador treinador;
    
    public PokemonFavorito() {
        super();
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public Treinador getTreinador() {
        return treinador;
    }

    public void setTreinador(Treinador treinador) {
        this.treinador = treinador;
    }
    
    
}
