/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author adams
 */
@Entity
@Table(name = "pokemons",
        indexes = {
            @Index(columnList = "especie")
        },
        uniqueConstraints = {
            @UniqueConstraint(columnNames = {"nome"})
        }
)
public class Pokemon extends AbstractEntity<Long> {

    @NotNull(message="{pokemon.nome.vazio}")
    @Size(min=3, max=64, message="{pokemon.nome.tamanho}")
    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @NotNull(message="{pokemon.especie.vazio}")
    @Size(min=3, max=64, message="{pokemon.especie.tamanho}")
    @Column(name = "especie", length = 64, nullable = false)
    private String especie;

    @NotNull(message="{pokemon.altura.vazio}")
    @Min(0)
    @Column(name = "altura", nullable = false)
    private double altura;

    @NotNull(message="{pokemon.peso.vazio}")
    @Min(0)
    @Column(name = "peso", nullable = false)
    private double peso;

    @NotNull(message="{pokemon.foto.vazio}")
    @Column(name = "foto", nullable = true)
    private String foto;

    //@NotNull(message="{pokemon.criador.vazio}")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "criador", nullable = false)
    private Treinador criador;

    public Pokemon() {
        super();
        this.nome = "Unnamed Pokemon";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Treinador getCriador() {
        return criador;
    }

    public void setCriador(Treinador criador) {
        this.criador = criador;
    }

    @Override
    public String toString() {
        return "Pokemon{" + "nome=" + nome + ", especie=" + especie + ", altura=" + altura + ", peso=" + peso + ", foto=" + foto + ", criador=" + criador + '}';
    }
        
}
