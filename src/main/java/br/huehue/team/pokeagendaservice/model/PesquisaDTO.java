/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.model;

import java.io.Serializable;

/**
 *
 * @author adams
 */
public class PesquisaDTO implements Serializable {

    private Long id;
    private String nome;
    private String especie;
    private double altura;
    private double peso;
    private String foto;
    private String criador;

    public PesquisaDTO() {

    }

    public PesquisaDTO(Long id, String nome, String especie, String foto, double altura, double peso, String criador) {
        this.id = id;
        this.nome = nome;
        this.especie = especie;
        this.foto = foto;
        this.altura = altura;
        this.peso = peso;
        this.criador = criador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public String getCriador() {
        return criador;
    }

    public void setCriador(String criador) {
        this.criador = criador;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    
}
