/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.huehue.team.pokeagendaservice.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author adams
 */
@Entity
@Table(name = "treinadores",
        indexes = {
            @Index(columnList = "nome"),
            @Index(columnList = "admin")
        },
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "login")
        }
)
public class Treinador extends AbstractEntity<Long> {

    @NotNull(message="{treinador.nome.vazio}")
    @Size(min=3, max=64, message= "{treinador.nome.tamanho}")
    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @NotNull(message="{treinador.login.vazio}")
    @Size(min=3, max=64, message="{treinador.login.tamanho}")
    @Column(name = "login", length = 64, nullable = false)
    private String login;

    @NotNull(message="{treinador.senha.vazio}")
    @Size(min=8, max=255, message="{treinador.senha.tamanho}")
    @Column(name = "senha", length = 255, nullable = false)
    private String senha;

    @Column(name = "admin", nullable = false)
    private boolean admin;

    @OneToMany(mappedBy = "criador")
    private List<Pokemon> criados;

    @OneToOne(mappedBy = "treinador")
    private PokemonFavorito favorito;
    
    public Treinador() {
        super();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public List<Pokemon> getCriados() {
        return criados;
    }

    public void setCriados(List<Pokemon> criados) {
        this.criados = criados;
    }

    public PokemonFavorito getFavorito() {
        return favorito;
    }

    public void setFavorito(PokemonFavorito favorito) {
        this.favorito = favorito;
    }
     
}
